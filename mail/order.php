<?php

use yii\helpers\Html;
use yii\helpers\Url; ?>

    <div class="responsive-table">
        <table class="table table-hover table-striped">
            <head>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>QTY</th>
                    <th>Sum</th>
                </tr>
            </head>
            <tbody>
            <?php foreach ($session['cart'] as $id => $item):?>
                <tr>
                    <td>
                        <a href="<?=Url::to(['product/view','id'=>$id], true);?>">
                        <?=$item['name'];?>
                        </a>
                    </td>
                    <td><?=$item['price'];?></td>
                    <td><?=$item['qty'];?></td>
                    <td><?=$item['qty'] * $item['price'];?></td>
                </tr>
            <?php endforeach;?>
            <tr>
                <td colspan="3">Qty:</td>
                <td><?=$session['cart.qty'];?></td>
            </tr>
            <tr>
                <td colspan="3">Sum:</td>
                <td><?=$session['cart.sum'];?></td>
            </tr>
            </tbody>
        </table>
    </div>

