<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 26.05.2018
 * Time: 18:05
 */

namespace app\models;
use yii\db\ActiveRecord;


class Category extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public static function tableName(){
        return 'category';
    }

    public function getProducts(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }


}