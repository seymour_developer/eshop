<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="container">
    <?php if(Yii::$app->session->hasFlash('success')) : ?>
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?=Yii::$app->session->getFlash('success');?>
        </div>
    <?php endif;?>

    <?php if(Yii::$app->session->hasFlash('error')) : ?>
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?=Yii::$app->session->getFlash('error');?>
        </div>
    <?php endif;?>

<?php if(!empty($session['cart'])):?>
    <div class="responsive-table">
        <table class="table table-hover table-striped">
            <head>
                <tr>
                    <th>Foto</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>QTY</th>
                    <th>Sum</th>
                    <th>
                        <span class="glyphicon glyphicon-remove" aria-hidde="true"></span>
                    </th>
                </tr>
            </head>
            <tbody>
            <?php foreach ($session['cart'] as $id => $item):?>
                <tr>
                    <td><?=Html::img("{$item['img']}",
                            ['alt' => $item['name'],'height' => 50]);?></td>
                    <td><a href="<?=Url::to(['product/view','id'=> $id])?>">
                            <?=$item['name'];?>
                        </a>
                    </td>
                    <td><?=$item['price'];?></td>
                    <td><?=$item['qty'];?></td>
                    <td><?=$item['qty'] * $item['price'];?></td>
                    <td>
                        <span data-id="<?=$id;?>" class="glyphicon glyphicon-remove del-item text-danger" aria-hidde="true" ></span>
                    </td>
                </tr>
            <?php endforeach;?>
            <tr>
                <td colspan="5">Qty:</td>
                <td><?=$session['cart.qty'];?></td>
            </tr>
            <tr>
                <td colspan="5">Sum:</td>
                <td><?=$session['cart.sum'];?></td>
            </tr>
            </tbody>
        </table>
        <hr>
        <?php $form = ActiveForm::begin();?>
            <?=$form->field($orderModel,'name');?>
            <?=$form->field($orderModel,'email');?>
            <?=$form->field($orderModel,'phone');?>
            <?=$form->field($orderModel,'address');?>
            <?=Html::submitButton('Order', ['class' => 'btn btn-success']);?>
        <?php $form->end();?>

    </div>
<?php else : ?>
    <h2>Cart is empty</h2>
<?php endif; ?>
</div>


